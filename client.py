#!/usr/bin/env python3
from message import Message
from message_catalog import MessageCatalog
import requests

class Client:
	#constructor, takes a message catalog
	def __init__(self, catalog, debug = False):
		pass ## TODO:


	#send an I said message to a host
	def say_something(self, host):
		pass ## TODO:

	#add to catalog all the covid patient messages from host server
	def get_covid(self, host):
		pass ## TODO:


	#send to a server the list of “I said” messages
	def send_history(self, host):
		pass ## TODO:

## TODO: créer une sous classe de client pour tester le programme en lui donnant une liste de particuliers et un serveur hopital : tous les "say_something" seront pour tous les particuliers et get_covid et send_history pour l'hopital. Idéalement cette classe enverra des messages toutes les 5 minutes, purgera sa liste de messages à intervalles réguliers et après avoir fait get_covid diagnostiquera si l'utilisateur est cas contact ou non.

if __name__ == "__main__":
	c = Client(MessageCatalog())
	c.say_something("http://localhost:5000")
	c.get_covid("http://localhost:5000")
	c.send_history("http://localhost:5000")
